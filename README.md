Description
===========

ddbuild aims to provide an sbuild alternative based on docker.


Installation
============
The sources embbed a debian packaging configuration.

For a manual installation, you can copy the complete sources in a directory.

Configuration
=============
The configuration is done in ddbuild.conf.

Usage
=====
ddbuild can build any package from its dsc file:
```
$ ddbuild my_super_package.dsc
```
or from the sources directory:
```
$ cd my_super_package
$ ddbuild --from-sources
```

Using QEMU for cross-compilation
================================
Docker can run images for another architecture using QEMU.

# Install the qemu packages
```
$ sudo apt-get install qemu binfmt-support qemu-user-static
```

# This step will execute the registering scripts
```
$ docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
```

# Run ddbuild forcing the architecture
```
$ ddbuild --arch arm64 my_super_package.dsc
```

