#!/usr/bin/env bats

setup() {
    # get the containing directory of this file
    # use $BATS_TEST_FILENAME instead of ${BASH_SOURCE[0]} or $0,
    # as those will point to the bats executable's location or the preprocessed file respectively
    DIR="$( cd "$( dirname "$BATS_TEST_FILENAME" )" >/dev/null 2>&1 && pwd )"

    # make executables in .. visible to PATH
    PATH="$DIR/../:$PATH"

    # create a temporary working dir
    BATS_TMPDIR="$(mktemp -d --tmpdir)"

    # hack to stop on first failed test
    [ ! -f ${BATS_PARENT_TMPNAME}.skip ] || skip "skip remaining tests"
}

teardown() {
    rm -rf "${BATS_TMPDIR}"

    # hack to stop on first failed test
    [ -n "$BATS_TEST_COMPLETED" ] || touch ${BATS_PARENT_TMPNAME}.skip
}

@test "build_from_sources" {
    cd ${DIR}/build_from_sources
    ddbuild --from-sources
    [ -f ${DIR}/build_from_sources/mydeb_1_all.deb ]
}

@test "build_from_dsc" {
    # prepare DSC from previous sources
    rm -fr ${DIR}/build_from_dsc
    mkdir ${DIR}/build_from_dsc
    cd ${DIR}/build_from_dsc
    cp -r ../build_from_sources ./mydeb
    pushd mydeb
    dpkg-buildpackage --build=source --no-sign
    popd
    # build the .deb
    ddbuild mydeb_1.dsc
    [ -f ${DIR}/build_from_dsc/mydeb_1_all.deb ]
}

@test "option_arch" {
    # test valid arch
    cd ${DIR}/build_from_sources
    ddbuild --from-sources --arch i386
    [ -f ${DIR}/build_from_sources/mydeb-arch_1_i386.deb ]
    ddbuild --from-sources --arch amd64
    [ -f ${DIR}/build_from_sources/mydeb-arch_1_amd64.deb ]
    ddbuild --from-sources --arch arm64
    [ -f ${DIR}/build_from_sources/mydeb-arch_1_arm64.deb ]

    # test invalid arch
    run ddbuild --from-sources --arch FOO
    [ "$status" -eq 1 ]
}

@test "build_from_sources_local" {
    rm -fr ${DIR}/build_from_sources_local
    cp -r ${DIR}/build_from_sources ${DIR}/build_from_sources_local
    cd ${DIR}/build_from_sources_local
    ddbuild --from-sources --local foo "foo bar"
    [ -f ${DIR}/build_from_sources_local/mydeb_1foo1_all.deb ]
}

@test "build_from_dsc_local" {
    # prepare DSC from previous sources
    rm -fr ${DIR}/build_from_dsc
    mkdir ${DIR}/build_from_dsc
    cd ${DIR}/build_from_dsc
    cp -r ../build_from_sources ./mydeb
    pushd mydeb
    dpkg-buildpackage --build=source --no-sign
    popd
    # build the .deb
    ddbuild mydeb_1.dsc --local foo "foo bar"
    [ -f ${DIR}/build_from_dsc/mydeb_1foo1_all.deb ]
}

@test "build_from_sources_bpo" {
    rm -fr ${DIR}/build_from_sources_bpo
    cp -r ${DIR}/build_from_sources ${DIR}/build_from_sources_bpo
    cd ${DIR}/build_from_sources_bpo
    ddbuild --from-sources --bpo
    [ -f ${DIR}/build_from_sources_bpo/mydeb_1~bpo11+1_all.deb ]
}

@test "build_from_dsc_bpo" {
    # prepare DSC from previous sources
    rm -fr ${DIR}/build_from_dsc
    mkdir ${DIR}/build_from_dsc
    cd ${DIR}/build_from_dsc
    cp -r ../build_from_sources ./mydeb
    pushd mydeb
    dpkg-buildpackage --build=source --no-sign
    popd
    # build the .deb
    ddbuild mydeb_1.dsc --bpo
    [ -f ${DIR}/build_from_dsc/mydeb_1~bpo11+1_all.deb ]
}
