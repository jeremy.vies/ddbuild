#!/bin/bash

# no error
set -e

TMPDIR=/tmp/ddbuild
TMPREPO=/tmp/repo
DSC=$1

PACKAGE=${DSC//.dsc}
PACKAGE=${PACKAGE:-$(basename "$PWD")}

# prepare logging
DATE=$(date -Iseconds)
LOG_FILE_TS=${PACKAGE}_${DATE}_build.log
LOG_FILE=${PACKAGE}_build.log

function pretty_trace() {
    echo
    echo   "+------------------------------------------------------------------------------+"
    printf "| %-76s |\n" "$1"
    echo   "+------------------------------------------------------------------------------+"
    echo
}

function prepare_extra_deb() {
    apt-get install -y reprepro
    mkdir ${TMPREPO}
    mkdir ${TMPREPO}/conf/
    cat > ${TMPREPO}/conf/distributions << EOF
Codename: ${DISTRIB}
Architectures: ${TARGET_ARCH}
Components: main
UDebComponents: main
Description: Temporary repository for ddbuild
EOF
    pushd ${TMPREPO}
    reprepro -b . export

    for P in /extra_packages/*.deb; do
        [[ -e "$P" ]] || break
        reprepro -vb . includedeb "${DISTRIB}" "$P"
    done

    popd

    echo "deb [trusted=yes] file://${TMPREPO}/ ${DISTRIB} main" >> /etc/apt/sources.list.d/extra.list
    apt-get update
}

function get_package_name() {
    NAME=$(grep -i "Source: " debian/control)
    echo "${NAME#"Source: "}"
}

pretty_trace "building ${LOG_FILE}"

echo "from here, build directory is replaced by <BUILD_ROOT> in log file."

# start a subshell to handle logging
(
pretty_trace "update container"

# update package list, and upgrade installation
apt-get update
apt-get dist-upgrade -y

pretty_trace "install basics packages for dev"

# install basics for building
# shellcheck disable=SC2086 # we want to split the variable
apt-get -y install --no-install-recommends ${BASIC_DEV_PACKAGES}

# check if a DSC file is provided
if [ -n "${DSC}" ] ; then
    pretty_trace "extract sources"

    # extract package
    dpkg-source -x "${DSC}" "${TMPDIR}"

    # get into extracted sources
    cd ${TMPDIR}/
fi

# check we are in debian package source
if [ ! -f "debian/control" ] ; then
    echo "this script should be run from a debian source dir"
    exit
fi

# define default email and fullname for changelog
[ -n "${DEBEMAIL}" ] || export DEBEMAIL="ddbuild@gitlab.com"
[ -n "${DEBFULLNAME}" ] || export DEBFULLNAME="ddbuild"

# handle bpo
[[ "${BPO}" == 1 ]] && dch --bpo ""

# add suffix if any
if [ -n "${LOCAL_SUFFIX}" ] ; then
    dch --local "${LOCAL_SUFFIX}" "${LOCAL_CHANGELOG}"
fi

pretty_trace "install additional packages"

# prepare extra packages
prepare_extra_deb

pretty_trace "install build dependencies"

# install dependencies
mk-build-deps --tool "apt-get -y --no-install-recommends -o Debug::pkgProblemResolver=yes" --install --remove "debian/control"

pretty_trace "build package"

# build binary package
# shellcheck disable=SC2086 # we want to split the variable
dpkg-buildpackage -b --target-arch "${TARGET_ARCH}" ${DPKG_BUILDPACKAGE_OPTIONS}

pretty_trace "copy results"

# copy results
cp ../*.deb /output
cp ../*.changes /output
cp ../*.buildinfo /output

# clean (only for build from source)
if [ $# -eq 0 ] ; then
    debian/rules clean
fi

# end of subshell
) >&1 2>&1 | sed -u -e "s%/tmp/build%<BUILD_ROOT>%g" | tee "/output/${LOG_FILE_TS}"

# link log file to new log file
cd /output
ln -sf "${LOG_FILE_TS}" "${LOG_FILE}"

